from typing import Optional

from fastapi import FastAPI

from pydantic import BaseModel

app = FastAPI()


class Mot(BaseModel):
    id:int
    caracteres:str
    def __init__(self,id,caracteres):
        self.id=id
        self.caracteres=caracteres


mots = [Mot(0, "coucou"), Mot(1, "Au-revoir")]

@app.get("/mots")
def read_mots():
    return mots


@app.post("/mot")
def add_mot(mot:Mot):
    mots.append(mot)
    return mots